//
//  main.cpp
//  Functor
//
//  Created by Ryza on 2016/8/4.
//  Copyright © 2016 Ryza. All rights reserved.
//

#include <iostream>
#include "Functor.hpp"

using std::cout;

int main(int argc, const char * argv[]) {
    Functor test = []() -> void {
        std::cout << "ax^2 + bx + c:\n";

        Functor formular = [](int a) -> Functor {
            cout << "a inited to " << a << '\n';
            return [a](int b) -> Functor {
                cout << "with a = " << a << ", b inited to " << b << '\n';
                return [a, b](int c) -> Functor {
                    cout << "with a = " << a << ", b = " << b << ", c inited to " << c << '\n';
                    return [a, b, c](int x) -> int {
                        return a*x*x + b*x + c;
                    };
                };
            };
        };


        int a = 1, b = 2, c = 3, x = 4;
        cout << a << '*' << x << '*' << x << '+' << b << '*' << x << '+' << c << '\n';

        Functor formular_with_a   = formular.as<Functor>()(a);
        Functor formular_with_ab  = formular_with_a.as<Functor>()(b);
        Functor formular_with_abc = formular_with_ab.as<Functor>()(c);
        cout << formular_with_abc.as<int>()(x) << '\n';

        cout << formular.as<Functor>()(a)
        .as<Functor>()(b)
        .as<Functor>()(c)
        .as<int>()(x) << '\n';
    };
    test.as<void>()();
    return 0;
}
